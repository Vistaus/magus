# Magus

Magus is a Wake-on-LAN application

## Building

You can build Magus with Gnome Builder, just clone the project and hit the run button.

### Requirements
- `meson >= 0.50`
- `gtk4 >= 4.0.0`
- `python3`
- `libadwaita1 >=1.0.0`
- `ninja`
- `pygobject >= 3.40.0`

### Building with Meson

```bash
git clone https://gitlab.com/guillermop/magus.git
cd magus
meson . _build --prefix=/usr
ninja -C _build
sudo ninja -C _build install
```
