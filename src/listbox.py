# listbox.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Magus.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Gio, GObject, Gdk
from .database import Database
from .row import Row


class ListBox(Gtk.ListBox):
    __gtype_name__ = 'ListBox'

    empty = GObject.Property(type=bool, default=True)
    has_selection = GObject.Property(type=bool, default=False)

    def __init__(self):
        super().__init__()
        self.model = Gio.ListStore()
        self.model.connect('items-changed', self.on_items_changed)
        self.bind_model(self.model, self.create_widget_function)
        controller = Gtk.GestureClick()
        controller.connect('released', self.on_button_release_event)
        self.add_controller(controller)
        self.connect_after('selected-rows-changed', self.on_selected_rows_changed)

    def on_items_changed(self, *args):
        self.props.empty = len(self.model) == 0

    def create_widget_function(self, machine) -> 'Row':
        row = Row(machine)
        window = self.get_root()
        window.bind_property('selection-mode', row, 'selection-mode')
        return row

    # def on_button_release_event(self, widget, event):
    def on_button_release_event(self, gesture, n, x, y):
        state = gesture.get_current_event_state()

        window = self.get_root()
        if not window.props.selection_mode:
            if state & Gdk.ModifierType.CONTROL_MASK or state & Gdk.ModifierType.SHIFT_MASK:
                window.props.selection_mode = True
            else:
                return

        row = self.get_row_at_y(y)
        if row:
            if row.props.selected:
                self.unselect_row(row)
                gesture.set_state(Gtk.EventSequenceState.CLAIMED)

    def on_selected_rows_changed(self, *args):
        has_selection = False
        for row in self:
            row.props.selected = row.is_selected()
            has_selection = row.props.selected or has_selection

        self.props.has_selection = has_selection

    def load_machines(self):
        for machine in Database.get_default().machines:
            self.model.append(machine)

    def get_selected_machines(self) -> {}:
        return {
            row.get_index(): row.machine for row in reversed(self.get_selected_rows())
        }

    def insert(self, machine, index=-1):
        if index < 0:
            self.model.append(machine)
        else:
            self.model.insert(index, machine)

    def remove(self, machine):
        position = self.model.find(machine).position
        self.model.remove(position)
