# dialog.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Magus.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, GObject, Gdk, Adw
from .utils import is_valid_ip, is_valid_mac, get_arp_table


@Gtk.Template(resource_path='/com/gitlab/guillermop/Magus/dialog.ui')
class Dialog(Adw.Window):
    __gtype_name__ = 'Dialog'

    __gsignals__ = {
        'response': (GObject.SIGNAL_RUN_FIRST, None, (int,))
    }

    edit_mode = GObject.Property(type=bool, default=False)

    deck: Adw.Leaflet = Gtk.Template.Child()
    arp_stack: Gtk.Stack = Gtk.Template.Child()
    name_entry: Gtk.Entry = Gtk.Template.Child()
    mac_entry: Gtk.Entry = Gtk.Template.Child()
    port_spin: Gtk.SpinButton = Gtk.Template.Child()
    broadcast_entry: Gtk.Entry = Gtk.Template.Child()
    save_button: Gtk.Button = Gtk.Template.Child()
    list_box: Gtk.ListBox = Gtk.Template.Child()

    def __init__(self, machine=None, **kwargs):
        super().__init__(**kwargs)
        self.machine = None
        if machine:
            self.machine = machine
            self._init_widgets()
            self.props.edit_mode = True

    def _init_widgets(self):
        self.name_entry.set_text(self.machine.props.name)
        self.mac_entry.set_text(self.machine.props.mac)
        self.port_spin.set_value(self.machine.props.port)
        self.broadcast_entry.set_text(self.machine.props.broadcast)

    def get_data(self):
        return [
            self.name_entry.get_text(),
            self.mac_entry.get_text().upper(),
            self.port_spin.get_value(),
            self.broadcast_entry.get_text()
        ]

    def _can_save(self) -> bool:
        name = self.name_entry.get_text()
        mac = self.mac_entry.get_text()
        broadcast = self.broadcast_entry.get_text()
        port = self.port_spin.get_value()

        changes = True
        if self.machine:
            changes = self.machine.props.port != port or \
                      self.machine.props.name != name or \
                      self.machine.props.mac != mac or \
                      self.machine.props.broadcast != broadcast

        valid = True

        if not name:
            self.name_entry.get_style_context().add_class('error')
            valid = False
        else:
            self.name_entry.get_style_context().remove_class('error')

        if not mac:
            valid = False
        elif not is_valid_mac(mac):
            self.mac_entry.get_style_context().add_class('error')
            valid = False
        else:
            self.mac_entry.get_style_context().remove_class('error')

        if not broadcast:
            valid = False
        elif not is_valid_ip(broadcast):
            self.broadcast_entry.get_style_context().add_class('error')
            valid = False
        else:
            self.broadcast_entry.get_style_context().remove_class('error')

        return changes and valid

    @Gtk.Template.Callback()
    def on_save_button_clicked(self, *args):
        self.emit('response', Gtk.ResponseType.OK)

    @Gtk.Template.Callback()
    def on_scan_button_clicked(self, *args):
        self.deck.navigate(1)
        self.refresh()

    @Gtk.Template.Callback()
    def on_back_button_clicked(self, *args):
        self.deck.navigate(0)

    @Gtk.Template.Callback()
    def update_response_buttons(self, *args):
        self.save_button.set_sensitive(self._can_save())

    def refresh(self):
        row = self.list_box.get_first_child()
        while row:
            temp = row.get_next_sibling()
            self.list_box.remove(row)
            row = temp
        for hostname, mac in get_arp_table():
            action_row = Adw.ActionRow(
                title=hostname,
                subtitle=mac,
                activatable=True,
                selectable=False,
            )
            action_row.add_suffix(
                Gtk.Image(
                    icon_name='go-next-symbolic'
                )
            )
            self.list_box.append(action_row)
            empty = False
        self.arp_stack.set_visible_child_name(
            'empty' if empty else 'list'
        )

    @Gtk.Template.Callback()
    def on_entry_activate(self, *args):
        if self.save_button.get_sensitive():
            self.emit('response', Gtk.ResponseType.OK)

    @Gtk.Template.Callback()
    def on_refresh_button_clicked(self, *args):
        self.refresh()

    @Gtk.Template.Callback()
    def on_key_press_event(self, event, keyval, keycode, state):
        if keyval == Gdk.KEY_Escape:
            self.emit('response', Gtk.ResponseType.CANCEL)
            return True
        return False

    @Gtk.Template.Callback()
    def on_row_activated(self, list_box, row):
        self.name_entry.set_text(row.get_title())
        self.mac_entry.set_text(row.get_subtitle())
        self.deck.navigate(0)
        self.name_entry.grab_focus()

