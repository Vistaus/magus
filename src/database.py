# database.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Magus.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import GObject, GLib
from os import path, makedirs
import sqlite3


class Database:

    instance: 'Database' = None

    def __init__(self):
        filename = path.join(
            GLib.get_user_data_dir(),
            'magus',
            'database.db'
        )
        makedirs(path.dirname(filename), exist_ok=True)
        self.connection = sqlite3.connect(filename)
        self.__create_tables()

    @staticmethod
    def get_default() -> 'Database':
        if Database.instance is None:
            Database.instance = Database()
        return Database.instance

    @property
    def machines(self) -> ['Machine']:
        query = 'SELECT * FROM machines'
        try:
            data = self.connection.cursor().execute(query)
            rows = data.fetchall()
            return [Machine(*machine) for machine in rows]
        except sqlite3.OperationalError as error:
            pass

    def insert_machine(self, *args) -> 'Machine':
        query = 'INSERT INTO machines VALUES (null, ?, ?, ?, ?, null, null, null, null)'
        cursor = self.connection.cursor()
        try:
            cursor.execute(query, args)
            self.connection.commit()
            return Machine(cursor.lastrowid, *args)
        except sqlite3.OperationalError as error:
            pass

    def delete_machine(self, id_: int):
        query = 'DELETE FROM machines WHERE id=?'
        try:
            self.connection.execute(query, [id_])
            self.connection.commit()
        except sqlite3.Error as error:
            pass

    def update_machine(self, id_: int, **kwargs):
        query = 'UPDATE machines SET '
        query += ', '.join([f'{column}=?' for column in kwargs.keys()])
        query += ' WHERE id=?'
        try:
            self.connection.execute(
                query,
                list(kwargs.values()) + [id_]
            )
            self.connection.commit()
        except sqlite3.OperationalError as error:
            pass

    def __create_tables(self):
        cursor = self.connection.cursor()
        cursor.executescript(
            '''
                CREATE TABLE IF NOT EXISTS "machines" (
                    "id"	INTEGER NOT NULL UNIQUE,
                    "name"	TEXT NOT NULL,
                    "mac"	TEXT NOT NULL,
                    "port"	INTEGER NOT NULL,
                    "broadcast"	TEXT NOT NULL,
                    PRIMARY KEY("id" AUTOINCREMENT)
                );
            '''
        )


class Machine(GObject.GObject):

    id_ = GObject.Property(type=int)
    name = GObject.Property(type=str)
    mac = GObject.Property(type=str)
    port = GObject.Property(type=int)
    broadcast = GObject.Property(type=str)

    def __init__(self, *args):
        super().__init__()
        self.props.id_ = args[0]
        self.props.name = args[1]
        self.props.mac = args[2]
        self.props.port = args[3]
        self.props.broadcast = args[4]

    @staticmethod
    def create(*args) -> 'Machine':
        return Database.get_default().insert_machine(*args)

    def delete(self):
        Database.get_default().delete_machine(self.props.id_)

    def update(self, name: str, mac: str, port: int, broadcast: str):
        self.props.name = name
        self.props.mac = mac
        self.props.port = port
        self.props.broadcast = broadcast

        Database.get_default().update_machine(
            self.props.id_,
            name=name,
            mac=mac,
            port=port,
            broadcast=broadcast
        )
