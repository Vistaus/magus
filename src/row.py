# row.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Magus.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Gio, Adw, GObject


@Gtk.Template(resource_path='/com/gitlab/guillermop/Magus/row.ui')
class Row(Adw.ActionRow):
    __gtype_name__ = 'Row'

    selection_mode = GObject.Property(type=bool, default=False)
    selected = GObject.Property(type=bool, default=False)

    check_button: Gtk.CheckButton = Gtk.Template.Child()
    wake_button: Gtk.Button = Gtk.Template.Child()
    more_button: Gtk.Button = Gtk.Template.Child()

    def __init__(self, machine):
        super().__init__(visible=True, activatable=False)
        self._action_group = Gio.SimpleActionGroup()
        self.insert_action_group('row', self._action_group)
        self.machine = machine
        self.bind_properties()
        self._setup_actions()

    def bind_properties(self):
        self.machine.bind_property(
            'name', self, 'title', GObject.BindingFlags.SYNC_CREATE
        )
        self.machine.bind_property(
            'mac', self, 'subtitle', GObject.BindingFlags.SYNC_CREATE
        )

    def _setup_actions(self):
        self._add_action('wake', self.wake)
        self._add_action('delete', self.delete)
        self._add_action('edit', self.edit)

    def _add_action(self, key, callback, bind=False):
        action = Gio.SimpleAction.new(key, None)
        action.connect("activate", callback)
        self._action_group.add_action(action)

    def wake(self, *args):
        self.get_root().wakeup_machines(self.machine)

    def delete(self, *args):
        self.get_root().delete_machines(
            {
                self.get_index(): self.machine
            }
        )

    def edit(self, *args):
        self.get_root().edit_machine(self.machine)

    @Gtk.Template.Callback()
    def on_selected_changed(self, *args):
        if self.props.selected:
            if not self.is_selected():
                self.get_parent().select_row(self)
        elif not self.check_button.get_active():
            self.get_parent().unselect_row(self)

    @Gtk.Template.Callback()
    def on_selection_mode_changed(self, *args):
        self.set_activatable(self.props.selection_mode)
        self.set_selectable(self.props.selection_mode)
        self.check_button.set_active(False)
