# window.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Magus.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Gio, GObject, Adw
from .listbox import ListBox
from .database import Machine
from .dialog import Dialog
from .utils import send_magic_packet
from gettext import gettext as _, ngettext


@Gtk.Template(resource_path='/com/gitlab/guillermop/Magus/window.ui')
class Window(Adw.ApplicationWindow):
    __gtype_name__ = 'Window'

    selection_mode = GObject.Property(type=bool, default=False)

    header_stack: Gtk.Stack = Gtk.Template.Child()
    main_stack: Gtk.Stack = Gtk.Template.Child()
    list_box: ListBox = Gtk.Template.Child()
    notification: Adw.Toast = None;
    overlay: Adw.ToastOverlay = Gtk.Template.Child()
    selection_button: Gtk.Label = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._setup_actions()
        self.list_box.load_machines()
        self._deleting = []

    def _setup_actions(self):
        self._add_action('add', self.add_machine)
        self._add_action('select-all', self.select_all, True)
        self._add_action('select-none', self.select_none, True)
        self._add_action('undo', self.undo)

    def _add_action(self, key, callback, bind=False):
        action = Gio.SimpleAction.new(key, None)
        action.connect("activate", callback)
        if bind:
            self.list_box.bind_property(
                "empty",
                action,
                "enabled",
                GObject.BindingFlags.INVERT_BOOLEAN | GObject.BindingFlags.SYNC_CREATE
            )
        super().add_action(action)

    def add_machine(self, *args):

        dialog = Dialog(
            title=_('Add a New Machine'),
            transient_for=self
        )

        def on_response(d, response):
            if response == Gtk.ResponseType.OK:
                machine = Machine.create(*d.get_data())
                self.list_box.insert(machine)
            d.destroy()

        dialog.connect("response", on_response)
        dialog.present()

    def wakeup_machines(self, *machines):
        send_magic_packet(*machines)

    def delete_machines(self, machines: dict):
        for position, machine in machines.items():
            self.list_box.remove(machine)
            self._deleting.append((position, machine))

        message = _('{0} machines deleted').format(len(self._deleting))
        if len(self._deleting) == 1:
            position, machine = self._deleting[0]
            message = _('"{0}" deleted').format(machine.props.name)

        if self.notification:
            self.notification.set_title(message)
            return

        self.notification = Adw.Toast(
            title=message,
            button_label=_('Undo'),
            action_name='win.undo',
            priority=Adw.ToastPriority.HIGH
        )

        def on_dismissed(*args):
            self.notification = None
            if self._deleting:
                for _, mac in self._deleting:
                    mac.delete()
                self._deleting = []

        self.notification.connect('dismissed', on_dismissed)
        self.overlay.add_toast(self.notification)

    def edit_machine(self, machine):
        dialog = Dialog(
            machine=machine,
            title=_('Edit Machine'),
            transient_for=self
        )

        def on_response(d, response):
            if response == Gtk.ResponseType.OK:
                machine.update(*d.get_data())
            d.destroy()

        dialog.connect("response", on_response)
        dialog.present()

    def select_all(self, *args):
        if not self.props.selection_mode:
            self.props.selection_mode = True
        self.list_box.select_all()

    def select_none(self, *args):
        self.list_box.unselect_all()

    def undo(self, *args):
        for index, machine in reversed(self._deleting):
            self.list_box.insert(machine, index)
        self._deleting = []

    @Gtk.Template.Callback()
    def on_empty_changed(self, *args):
        self.props.selection_mode = False
        self.main_stack.set_visible_child_name(
            'empty' if self.list_box.props.empty else 'default'
        )

    @Gtk.Template.Callback()
    def on_wake_button_clicked(self, *args):
        machines = list(self.list_box.get_selected_machines().values())
        self.wakeup_machines(*machines)
        self.props.selection_mode = False

    @Gtk.Template.Callback()
    def on_delete_button_clicked(self, *args):
        self.delete_machines(
            self.list_box.get_selected_machines()
        )

    @Gtk.Template.Callback()
    def on_cancel_button_clicked(self, *args):
        self.props.selection_mode = False

    @Gtk.Template.Callback()
    def on_selection_mode_changed(self, *args):
        self.list_box.unselect_all()
        self.header_stack.set_visible_child_name(
            'selection' if self.props.selection_mode else 'default'
        )

    @Gtk.Template.Callback()
    def on_destroy(self, *args):
        if self.notification:
            self.notification.dismiss()

    @Gtk.Template.Callback()
    def on_selected_rows_changed(self, *args):
        selected = len(self.list_box.get_selected_rows())
        if selected == 0:
            text = _('Click on items to select them')
        else:
            text = ngettext(
                "{} selected machine",
                "{} selected machines",
                selected).format(selected)

        self.selection_button.set_label(text)
