# main.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Magus.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Gio, GLib, Adw
from .window import Window
from .settings import Settings
from .preferences import Preferences
from gettext import gettext as _


class Application(Adw.Application):
    def __init__(self, version, application_id):
        self.version = version
        super().__init__(
            flags=Gio.ApplicationFlags.FLAGS_NONE,
            application_id=application_id
        )
        settings = Settings(application_id)
        settings.connect("changed::dark-theme", self.on_dark_theme_changed)

    def do_startup(self):
        Adw.Application.do_startup(self)
        GLib.set_application_name(_('Magus'))
        self._add_action("quit", self.on_quit)
        self._add_action("about", self.on_about)
        self._add_action("preferences", self.on_preferences)
        self.set_accels_for_action("app.preferences", ["<Ctrl>comma"])
        self.set_accels_for_action("app.quit", ["<Ctrl>Q"])
        self.set_accels_for_action("win.add", ["<Ctrl>N"])
        self.set_accels_for_action("win.select-all", ["<Ctrl>A"])
        self.on_dark_theme_changed()

    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = Window(application=self)
            if self.props.application_id.endswith('Devel'):
                win.get_style_context().add_class('devel')
        win.present()

    def _add_action(self, key, callback):
        action = Gio.SimpleAction.new(key, None)
        action.connect("activate", callback)
        super().add_action(action)

    def on_dark_theme_changed(self, *_):
        color = Adw.ColorScheme.PREFER_LIGHT
        if Settings.get_default().props.dark_theme:
            color = Adw.ColorScheme.PREFER_DARK
        Adw.StyleManager.get_default().set_property(
            'color-scheme',
            color
        )

    def on_preferences(self, *_):
        preferences = Preferences()
        preferences.set_transient_for(self.props.active_window)
        preferences.present()

    def on_about(self, *args):
        builder = Gtk.Builder.new_from_resource(
            '/com/gitlab/guillermop/Magus/about.ui')
        about = builder.get_object('about_dialog')
        about.set_transient_for(self.props.active_window)
        about.set_logo_icon_name(self.get_application_id())
        about.set_version(self.version)
        about.present()

    def on_quit(self, *args):
        self.props.active_window.close()
        self.quit()
