# utils.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Magus.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import socket
import os
import re
import ipaddress


ARP_PATH = '/proc/net/arp'


def is_valid_mac(mac):
    return re.match(
        '^([\\dA-F]{2}:){5}([\\dA-F]{2})$',
        mac.upper()
    )


def is_valid_ip(ip):
    try:
        ipaddress.ip_address(ip)
    except ValueError:
        return False
    return True


def send_magic_packet(*machines):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

    for machine in machines:
        address = (machine.props.broadcast, machine.props.port)
        payload = bytes.fromhex(
            f'FFFFFFFFFFFF{machine.props.mac.replace(":", "")*16}'
        )
        sock.sendto(payload, (machine.props.broadcast, machine.props.port))


def get_arp_table():
    if not os.path.exists(ARP_PATH):
        return
    with open(ARP_PATH) as f:
        f.readline()
        line = f.readline()
        while line:
            columns = line.split()
            ip, mac = columns[0], columns[3]
            if mac == '00:00:00:00:00:00':
                continue
            yield socket.getfqdn(ip) or ip, mac.upper()
            line = f.readline()

